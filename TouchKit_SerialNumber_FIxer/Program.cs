﻿using Dapper;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using warranty_contract_source_reference.Interfaces;
using warranty_contract_source_reference.Models;

namespace TouchKit_SerialNumber_FIxer
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = CreateClient();
            var database = client.GetDatabase("WarrantiesContracts");

            BsonClassMap.RegisterClassMap<ParsedWarranty>();
            BsonClassMap.RegisterClassMap<Meta>();
            BsonClassMap.RegisterClassMap<Error>();
            BsonClassMap.RegisterClassMap<Note>();
            BsonClassMap.RegisterClassMap<Account>();
            BsonClassMap.RegisterClassMap<Site>();
            BsonClassMap.RegisterClassMap<Asset>();
            BsonClassMap.RegisterClassMap<Contract>();
            BsonClassMap.RegisterClassMap<Installer>();
            BsonClassMap.RegisterClassMap<InstallerContact>();
            BsonClassMap.RegisterClassMap<Address>();

            // Find all errored records where product code is not recognised
            var errorFilter = Builders<IParsedWarranty>.Filter.Eq("Meta.Status", "Errored");
            var errorMessageFilter = Builders<IParsedWarranty>.Filter.Eq("Meta.Errors.Description", "Product Code is not recognised");
            var controlFilter = Builders<IParsedWarranty>.Filter.Eq("Assets.ProductType", "Control");

            var filter = Builders<IParsedWarranty>.Filter.And(errorFilter, errorMessageFilter, controlFilter);

            var results = database.GetCollection<IParsedWarranty>("WarrantiesContractsProcessing").Find(filter).ToList();

            // loop through all objects
            foreach (var item in results)
            {
                // Find the control asset
                var control = item.Assets.FirstOrDefault(x => x.ProductType == "Control") as Asset;

                // Find the Kit SN
                string kitSerialNumber = GetKitSN(control.SerialNumber);

                if (!string.IsNullOrEmpty(kitSerialNumber))
                {
                    control.SerialNumber = kitSerialNumber;
                    control.ProductCode = kitSerialNumber.Substring(0, 6);

                    var errorToResolve = item.Meta.Errors.FirstOrDefault(x => !x.IsResolved && x.Description == "Product Code is not recognised");

                    errorToResolve.DateResolved = DateTime.Now;
                    errorToResolve.IsResolved = true;

                    item.Meta.Status = "Imported";

                    // write it back to database
                    var update = Builders<ParsedWarranty>.Update.Set("Account", item.Account)
                                                 .Set("Site", item.Site)
                                                 .Set("Assets", item.Assets)
                                                 .Set("Installer", item.Installer)
                                                 .Set("Contract", item.Contract)
                                                 .Set("Meta", item.Meta);

                    var filter2 = Builders<ParsedWarranty>.Filter.Eq("_id", item.Id);
                    var options = new FindOneAndUpdateOptions<ParsedWarranty> { IsUpsert = false, ReturnDocument = ReturnDocument.After };
                    database.GetCollection<ParsedWarranty>("WarrantiesContractsProcessing").FindOneAndUpdate(filter2, update, options);
                }
            }

            Console.ReadLine();
        }

        private static string GetKitSN(string serialNumber)
        {
            string returnSerialNumber;

            using (IDbConnection db = new SqlConnection("Server=hullsql01\\prod;Database=ICPRODUCTDATABASE;User Id=intranet;Password=quintos31;"))
            {
                var results = db.Query<TouchSerialNumberHolder>($"Select KitSN from tbl_TouchSerialNo where SerNo = {serialNumber}");
                db.Close();

                if (!results.Any())
                {
                    return null;
                }

                returnSerialNumber = results.FirstOrDefault().KitSN;
            }

            return returnSerialNumber;
            
        }

        public static MongoClient CreateClient()
        {
            var client = new MongoClient("mongodb://warconloa:S16GDBA3GnBm56hk@10.200.151.35");

            return client;
        }

        public class TouchSerialNumberHolder
        {
            public string KitSN { get; set; }
        }
    }
}